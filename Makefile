dev_run:
	docker-compose -f docker-compose-dev.yml build
	docker-compose -f docker-compose-dev.yml up -d

deploy:
	docker-compose -f docker-compose-prod.yml build
	docker-compose -f docker-compose-prod.yml up -d

create_su_prod:
	docker-compose -f docker-compose-prod.yml run --rm django python manage.py createsuperuser

create_su_dev:
	docker-compose -f docker-compose-dev.yml run --rm django python manage.py createsuperuser

stop_dev:
	docker-compose -f docker-compose-dev.yml stop && docker-compose -f docker-compose-dev.yml rm -f

stop_prod:
	docker-compose -f docker-compose-prod.yml stop