# CSV Generator

## List of env variables to set-up:
- For Django:
    - DEBUG (default=False)
    - SECRET_KEY (default= Django generated secret key)
    - DJANGO_ALLOWED_HOSTS (default=[])
    - DJANGO_SETTINGS_MODULE (default=config.settings)
    - DATABASE_URL 
  (in format : postgresql://db_user:db_pass@db:5432/db_name)
- For postgres db:
    - POSTGRES_USER
    - POSTGRES_PASSWORD
    - POSTGRES_DB
- For redis:
    - REDIS_HOST (default=localhost)
    - REDIS_PORT (default=6379)

Also Celery settings are located in config.settings.


### Before do something:
1. ```$ cp .env.tpl .env```
2. Set-up env variables in .env file

****
### To run project locally:
+ One way:
  + ```$ make dev_run```
+ Second way:
  1. ```docker-compose -f docker-compose-dev.yml build```
  2. ``docker-compose -f docker-compose-dev.yml up -d``

### To stop project locally:
+ One way :D
  + ```make stop_dev``` (includes auto-deleting containers after stopping)
+ Second way:
  + ```docker-compose -f docker-compose-dev.yml stop```

***
### To run project in production:
+ One way:
  + ```$ make deploy```
+ Second way:
  1. ```docker-compose -f docker-compose-prod.yml build```
  2. ```docker-compose -f docker-compose-prod.yml up -d```

### To stop project in production:
+ One way :D
  + ```make stop_prod```
+ Second way:
  + ```docker-compose -f docker-compose-prod.yml stop```

***
### Execute Management Commands:
+ To make migration:
  + ```docker-compose -f your_docker_compose_file.yml run --rm django python manage.py migrate```
#### To create superuser:
+ One way:
  + ```docker-compose -f your_docker_compose_file.yml run --rm django python manage.py createsuperuser```
+ Second way:
  + prod: ```make create_su_prod```
  + dev: ```make create_su_dev```